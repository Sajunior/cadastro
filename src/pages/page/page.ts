import { LoginPage } from './../login/login';
import { Component, Input } from '@angular/core';
import { NavController } from 'ionic-angular';

@Component({
  selector: 'page-page',
  templateUrl: 'page.html'
})
export class PagePage {

  @Input('ngModelOptions')
  options: {
      standalone?: boolean;
  }

  gaming: any
  pasta: any

  games = [{nome: "dos"},{nome: "outro"}, {nome:"ps4"}]

  button = [{nome: "dos"},{nome: "outro"}, {nome:"ps4"}]

  constructor(public navCtrl: NavController) {

  }

  f(){
    ///this.navCtrl.push(LoginPage)
    console.log(this.gaming)
  }
  

  goToLoginPage(){
    this.navCtrl.push(LoginPage);
  }
  
}
